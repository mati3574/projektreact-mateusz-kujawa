import React from 'react';

class ProductRow extends React.Component {
    render() {
      const product = this.props.product;
      const name = product.stocked ?
        product.name :
        <span style={{color: 'red'}}>
          {product.name}
        </span>;
  
      return (
        <tr className="border-b border-green-700">
          <td className="p-1">{name}</td>
          <td className="p-1 text-right">{product.price}</td>
          <td className="p-1 text-right">{product.weight}</td>
        </tr>
      );
    }
  }

  export default ProductRow;