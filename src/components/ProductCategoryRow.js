import React from 'react';

class ProductCategoryRow extends React.Component {
    render() {
      const category = this.props.category;
      return (
        <tr>
          <th colSpan="3" className="text-green-200 bg-green-800 border-b border-green-700 py-3">
            {category}
          </th>
        </tr>
      );
    }
  }

  export default ProductCategoryRow;