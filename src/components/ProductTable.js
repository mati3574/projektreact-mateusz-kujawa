import React from 'react';
import ProductCategoryRow from './ProductCategoryRow';
import ProductRow from './ProductRow';


class ProductTable extends React.Component {
    render() {
      const filterText = this.props.filterText;
      const inStockOnly = this.props.inStockOnly;
  
      const rows = [];
      let lastCategory = null;
  
      this.props.products.forEach((product) => {
        if (product.name.indexOf(filterText) === -1) {
          return;
        }
        if (inStockOnly && !product.stocked) {
          return;
        }
        if (product.category !== lastCategory) {
          rows.push(
            <ProductCategoryRow
              category={product.category}
              key={product.category} />
          );
        }
        rows.push(
          <ProductRow
            product={product}
            key={product.name}
          />
        );
        lastCategory = product.category;
      });
  
      return (
        <table className="table-fixed lg:w-1/3 md:w-1/3 sm:w-1/2 my-5">
          <thead>
            <tr className="border-b border-t border-green-700">
              <th className="w-1/2 px-4 py-2 text-left">Nazwa</th>
              <th className="w-1/2 px-4 py-2 text-right">Cena</th>
              <th className="w-1/2 px-4 py-2 text-right">Pojemność/Waga</th>
            </tr>
          </thead>
          <tbody>{rows}</tbody>
        </table>
      );
    }
  }

  export default ProductTable;