import React from 'react';

class SearchBar extends React.Component {
    constructor(props) {
      super(props);
      this.handleFilterTextChange = this.handleFilterTextChange.bind(this);
      this.handleInStockChange = this.handleInStockChange.bind(this);
    }
    
    handleFilterTextChange(e) {
      this.props.onFilterTextChange(e.target.value);
    }
    
    handleInStockChange(e) {
      this.props.onInStockChange(e.target.checked);
    }
    
    render() {
      return (
        <form>
          <input
            type="text"
            placeholder="Szukaj..."
            value={this.props.filterText}
            onChange={this.handleFilterTextChange}
            className="shadow appearance-none border-none rounded w-full py-2 px-3 mb-4 bg-green-100 leading-tight focus:outline-none focus:shadow-outline"
          />
          <p>
            <input
              type="checkbox"
              checked={this.props.inStockOnly}
              onChange={this.handleInStockChange}
              className="mr-2 leading-tight"
            />
        <span class="text-sm">
            Wyświetl tylko dostępne produkty
        </span>
          </p>
        </form>
      );
    }
  }

  export default SearchBar;