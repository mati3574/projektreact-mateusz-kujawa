import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import ProductCategoryRow from './components/ProductCategoryRow';
import ProductRow from './components/ProductRow';
import ProductTable  from './components/ProductTable';
import SearchBar from './components/SearchBar';
import FilterableProductTable from './components/FilterableProductTable';


<div>
  <FilterableProductTable />

  <SearchBar />
  <ProductTable />
  <ProductCategoryRow />  
  <ProductRow />
</div>


const PRODUCTS = [
  {category: 'Kawa', price: '13.99 zł', stocked: true, name: 'Espresso', weight: '50ml'},
  {category: 'Kawa', price: '15.99 zł', stocked: true, name: 'Cappuccino', weight: '400ml'},
  {category: 'Kawa', price: '15.99 zł', stocked: false, name: 'Latte', weight: '400ml'},
  {category: 'Kawa', price: '17.99 zł', stocked: false, name: 'Breve', weight: '500ml'},
  {category: 'Herbata', price: '8 zł', stocked: true, name: 'Zielona', weight: '250ml'},
  {category: 'Herbata', price: '10 zł', stocked: false, name: 'Biała', weight: '250ml'},
  {category: 'Herbata', price: '7 zł', stocked: true, name: 'Czarna', weight: '250ml'},
  {category: 'Herbata', price: '9 zł', stocked: false, name: 'Owocowa', weight: '250ml'},
  {category: 'Dodatki', price: '3 zł', stocked: false, name: 'Ciastko zbożowe', weight: '30g'},
  {category: 'Dodatki', price: '3 zł', stocked: false, name: 'Ciastko korzenne', weight: '30g'},
  {category: 'Dodatki', price: '10 zł', stocked: true, name: 'Karpatka', weight: '150g'},
  {category: 'Dodatki', price: '12 zł', stocked: true, name: 'Ciasto owocowe', weight: '150g'},

];

ReactDOM.render(
  <FilterableProductTable products={PRODUCTS} />,
  document.getElementById('root')
);
